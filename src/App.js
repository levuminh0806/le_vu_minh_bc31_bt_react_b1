import logo from "./logo.svg";
import "./App.css";
import HeaderComponent from "./BaiTapLayoutCompunent/BaiTapThucHanhLayOut/HeaderComponent";
import BannerComponent from "./BaiTapLayoutCompunent/BaiTapThucHanhLayOut/BannerComponent";
import ItemComponent from "./BaiTapLayoutCompunent/BaiTapThucHanhLayOut/ItemComponent";
import FooterComponent from "./BaiTapLayoutCompunent/BaiTapThucHanhLayOut/FooterComponent";
import Style from "./Style/Style";
function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <BannerComponent />
      <ItemComponent />
      <FooterComponent />
      <Style />
    </div>
  );
}

export default App;
