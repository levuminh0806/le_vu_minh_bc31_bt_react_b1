import React, { Component } from "react";

export default class BannerComponent extends Component {
  render() {
    let title = "A warm welcome!";
    let btn = "Call To Action";
    let text =
      "Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?";
    return (
      <div>
        <div className="  banner-card container">
          <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">{text}</p>
            <a href="#" className="btn btn-primary">
              {btn}
            </a>
          </div>
        </div>
      </div>
    );
  }
}
