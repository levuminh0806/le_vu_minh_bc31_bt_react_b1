import React, { Component } from "react";

export default class HeaderComponent extends Component {
  render() {
    return (
      <div className="hearder">
        <div className="container">
          <header className="banner">
            <h3> Start Boostrap </h3>
            <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#" className="text__hover">
                  About
                </a>
              </li>
              <li>
                <a href="#" className="text__hover">
                  Contact
                </a>
              </li>
            </ul>
          </header>
        </div>
      </div>
    );
  }
}
