import React, { Component } from "react";

export default class FooterComponent extends Component {
  render() {
    return (
      <div>
        <div className="footer">
          <p>Copyright © Your Website 2022</p>
        </div>
      </div>
    );
  }
}
